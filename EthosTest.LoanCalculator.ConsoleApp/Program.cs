﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EthosTest.LoanCalculator.ConsoleApp.Infrastructure;
using EthosTest.LoanCalculator.Modules;
using Newtonsoft.Json;
using Ninject;

namespace EthosTest.LoanCalculator.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // configure boilerplate stuff
            var kernel = new StandardKernel(new LoanCalculatorModule());
            var automapperConfig = new MapperConfiguration(cfg => {
                cfg.AddProfile<LoanCalculatorProfile>();
            });
            var mapper = automapperConfig.CreateMapper();
            var serializer = new JsonSerializer();
            serializer.Converters.Add(new DecimalJsonConverter());
            serializer.Formatting = Formatting.Indented;

            var calculator = kernel.Get<ILoanCalculator>();

            object result;
            try
            {
                using (var standartInput = Console.OpenStandardInput())
                using (var standartOutput = Console.OpenStandardOutput())
                using (var reader = new LoanInputReader(standartInput, standartOutput))
                {
                    // read inputs
                    reader.ReadInputs();

                    // calculate loan
                    var loanCalculation = calculator.CalculateLoan(
                        reader.Amount.Value,
                        reader.Intereset,
                        reader.Downpayment.Value,
                        reader.Term);

                    // map to dto
                    result = mapper.Map<CalculationResultDto>(loanCalculation);
                }
            }
            catch (Exception ex)
            {
                result = new ErrorDto
                {
                    Error = ex.Message
                };
            }

            // output the result
            using (var stream = Console.OpenStandardOutput())
            using (var sw = new StreamWriter(stream))
            {
                sw.AutoFlush = true;
                serializer.Serialize(sw, result);
                sw.WriteLine();
            }
        }
    }
}
