﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EthosTest.LoanCalculator.ConsoleApp
{
    /// <summary>
    /// Represents the loan calculation result
    /// </summary>
    public class CalculationResultDto
    {
        /// <summary>
        /// MonthlyPayment
        /// </summary>
        [JsonProperty("monthly payment")]
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// TotalInterest
        /// </summary>
        [JsonProperty("total interest")]
        public decimal TotalInterest { get; set; }

        /// <summary>
        /// TotalPayment
        /// </summary>
        [JsonProperty("total payment")]
        public decimal TotalPayment { get; set; }
    }
}
