﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace EthosTest.LoanCalculator.ConsoleApp.Infrastructure
{
    class LoanCalculatorProfile : Profile
    {
        public LoanCalculatorProfile()
        {
            CreateMap<ILoanCalculation, CalculationResultDto>();
        }
    }
}
