﻿using Newtonsoft.Json;

namespace EthosTest.LoanCalculator.ConsoleApp
{
    /// <summary>
    /// Represents the loan calculation error
    /// </summary>
    public class ErrorDto
    {
        /// <summary>
        /// String containing the error description
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}