﻿using System;
using System.Globalization;
using System.IO;
using EthosTest.LoanCalculator.Units;

namespace EthosTest.LoanCalculator.ConsoleApp
{
    internal class LoanInputReader : IDisposable
    {
        private readonly StreamReader _streamReader;
        private readonly StreamWriter _streamWriter;

        public LoanInputReader(Stream inputStream, Stream outputStream)
        {
            _streamReader = new StreamReader(inputStream);
            _streamWriter = new StreamWriter(outputStream);
            _streamWriter.AutoFlush = true;
        }

        public void ReadInputs()
        {
            DoRead("amount: ", TryWrapper(ReadAmount));
            DoRead("interest: ", TryWrapper(ReadInterest));
            DoRead("downpayment: ", TryWrapper(ReadDownpayment));
            DoRead("term: ", TryWrapper(ReadTerm));
            _streamReader.ReadLine();
        }

        private void DoRead(string caption, Func<string, bool> readDelegate)
        {
            bool isSuccess = false;
            do
            {
                _streamWriter.Write(caption);
                var line = _streamReader.ReadLine();
                isSuccess = readDelegate(line);
            }
            while(!isSuccess);
        }

        private void ReadTerm(string s)
        {
            Term = Term.FromYears(ParseInt(s));
        }

        private void ReadDownpayment(string s)
        {
            Downpayment = ParseDecimal(s);
        }

        private void ReadInterest(string s)
        {
            Intereset = Percent.FromPercent(ParseDecimal(s));
        }

        private void ReadAmount(string s)
        {
            Amount = ParseDecimal(s);
        }

        private decimal ParseDecimal(string s)
        {
            return decimal.Parse(s.Replace(" ", "").Replace("%", ""), NumberStyles.Any);
        }

        private int ParseInt(string s)
        {
            return int.Parse(s.Replace(" ", "").Replace("%", ""));
        }

        private Func<string, bool> TryWrapper(Action<string> action)
        {
            return t =>
            {
                try
                {
                    action(t);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public decimal? Amount { get; private set; }
        public Percent Intereset { get; private set; }
        public decimal? Downpayment { get; private set; }
        public Term Term { get; private set; }

        public void Dispose()
        {
            _streamReader?.Dispose();
            _streamWriter?.Dispose();
        }
    }
}