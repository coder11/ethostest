﻿using System;
using EthosTest.LoanCalculator.Units;
using NUnit.Framework;
using Calculator = EthosTest.LoanCalculator.LoanCalculator;

namespace EthosTest.LoanCalculator.Test
{
    
    class LoanCalculatorTests
    {
        const decimal Delta = 0.005m;

        [Test]
        public void Calculator_should_do_test_assignment_calculation_correctly()
        {
            // arrange
            var calculator = new Calculator();

            // act 
            var calculation = calculator.CalculateLoan(100000m, Percent.FromPercent(5.5m), 20000m, Term.FromYears(30));

            // assert
            AssertDecimalsEqual(454.23m, calculation.MonthlyPayment);
            AssertDecimalsEqual(83523.23m, calculation.TotalInterest);
            AssertDecimalsEqual(163523.23m, calculation.TotalPayment);
        }

        [Test]
        public void Calculator_should_not_allow_calculation_for_invalid_parameters()
        {
            // arrange
            var calculator = new Calculator();

            // act 
            TestDelegate invalidAmount = () => calculator.CalculateLoan(-10, Percent.FromPercent(5.5m), 20000m, Term.FromYears(30));
            TestDelegate invalidDownpayment = () => calculator.CalculateLoan(10, Percent.FromPercent(5.5m), -10, Term.FromYears(30));
            TestDelegate invalidTerm = () => calculator.CalculateLoan(10, Percent.FromPercent(5.5m), 0, Term.FromYears(0));
            TestDelegate invalidInterest = () => calculator.CalculateLoan(10, Percent.FromPercent(0), 0, Term.FromYears(10));
            TestDelegate downpaymentMoreThanAmount = () => calculator.CalculateLoan(10, Percent.FromPercent(5.5m), 100, Term.FromYears(30));
            TestDelegate downpaymentMoreThanAmount2 = () => calculator.CalculateLoan(10, Percent.FromPercent(5.5m), 10, Term.FromYears(30));

            // assert
            Assert.Throws<ArgumentException>(invalidAmount);
            Assert.Throws<ArgumentException>(invalidDownpayment);
            Assert.Throws<ArgumentException>(invalidTerm);
            Assert.Throws<ArgumentException>(invalidInterest);
            Assert.Throws<ArgumentException>(downpaymentMoreThanAmount);
            Assert.Throws<ArgumentException>(downpaymentMoreThanAmount2);
        }

        private void AssertDecimalsEqual(decimal expected, decimal actual, decimal delta = Delta)
        {
            Assert.That(Math.Abs(expected - actual), Is.LessThan(delta));
        }
    }
}
