﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EthosTest.LoanCalculator.Units;
using NUnit.Framework;

namespace EthosTest.LoanCalculator.Test
{
    class PercentTests
    {
        [Test]
        public void Percent_should_be_correctly_constructed_from_percent()
        {
            // arrange
            var rawPercent = 5.5m;

            // act
            var percent = Percent.FromPercent(rawPercent);

            // assert
            Assert.That(percent.AsPercent, Is.EqualTo(rawPercent));
            Assert.That(percent.AsRatio, Is.EqualTo(0.055));
        }

        [Test]
        public void Percent_should_be_correctly_constructed_from_ratio()
        {
            // arrange
            var rawRatio = 0.554m;

            // act
            var percent = Percent.FromRatio(rawRatio);

            // assert
            Assert.That(percent.AsPercent, Is.EqualTo(55.4m));
            Assert.That(percent.AsRatio, Is.EqualTo(rawRatio));
        }

        [Test]
        [TestCase(-1)]
        [TestCase(101)]
        public void When_constructing_Percent_from_percent_with_invalid_data_exception_should_be_thrown(decimal argument)
        {
            // arrange

            // act
            TestDelegate action = () => Percent.FromPercent(argument);

            // assert
            Assert.Throws<ArgumentException>(action);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(1.1)]
        public void When_constructing_Percent_from_ratio_with_invalid_data_exception_should_be_thrown(decimal argument)
        {
            // arrange

            // act
            TestDelegate action = () => Percent.FromRatio(argument);

            // assert
            Assert.Throws<ArgumentException>(action);
        }
    }
}
