﻿using System;
using EthosTest.LoanCalculator.Units;
using NUnit.Framework;

namespace EthosTest.LoanCalculator.Test
{
    class TermTests
    {
        [Test]
        public void Term_should_be_correctly_constructed_from_years()
        {
            // arrange
            var years = 30;

            // act
            var term = Term.FromYears(years);

            // assert
            Assert.That(term.FullYears, Is.EqualTo(years));
            Assert.That(term.TotalMonths, Is.EqualTo(30 * 12));
            Assert.That(term.RemainingMonths, Is.EqualTo(0));
        }

        [Test]
        public void Term_should_be_correctly_constructed_from_even_number_of_months()
        {
            // arrange
            var months = 30 * 12;

            // act
            var term = Term.FromMonths(months);

            // assert
            Assert.That(term.FullYears, Is.EqualTo(30));
            Assert.That(term.TotalMonths, Is.EqualTo(months));
            Assert.That(term.RemainingMonths, Is.EqualTo(0));
        }

        [Test]
        public void Term_should_be_correctly_constructed_from_odd_number_of_months()
        {
            // arrange
            var months = 30 * 12 + 5;

            // act
            var term = Term.FromMonths(months);

            // assert
            Assert.That(term.FullYears, Is.EqualTo(30));
            Assert.That(term.TotalMonths, Is.EqualTo(months));
            Assert.That(term.RemainingMonths, Is.EqualTo(5));
        }

        [Test]
        public void When_constructing_Term_with_invalid_data_exception_should_be_thrown()
        {
            // arrange
            var negativeNumber = -1;

            // act
            TestDelegate fromMonths = () => Term.FromMonths(negativeNumber);
            TestDelegate fromYears = () => Term.FromYears(negativeNumber);

            // assert
            Assert.Throws<ArgumentException>(fromMonths);
            Assert.Throws<ArgumentException>(fromYears);
        }
    }
}