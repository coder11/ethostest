﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace EthosTest.LoanCalculator.Modules
{
    public class LoanCalculatorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILoanCalculator>().To<LoanCalculator>();
        }
    }
}
