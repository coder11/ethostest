﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using EthosTest.LoanCalculator.Units;

namespace EthosTest.LoanCalculator
{
    internal class LoanCalculator : ILoanCalculator
    {
        public ILoanCalculation CalculateLoan(decimal amount, Percent interest, decimal downpayment, Term term)
        {
            if (amount < 0)
            {
                throw new ArgumentException("Amount must be a non negative number", nameof(amount));
            }

            if (downpayment < 0)
            {
                throw new ArgumentException("Downpayment must be a non negative number", nameof(downpayment));
            }

            if (term.TotalMonths == 0)
            {
                throw new ArgumentException("Term must be more than 0 months", nameof(term));
            }

            if (interest.AsRatio == 0)
            {
                throw new ArgumentException("Intereset must be more than 0", nameof(term));
            }

            if (downpayment >= amount)
            {
                throw new ArgumentException("Downpayment must not be more than amount");
            }

            var monthlyInterest  = interest.AsRatio / 12;

            var totalLoan = amount - downpayment;
            var annuityFactor = 1 - (decimal) Math.Pow(1 + (double) monthlyInterest, -term.TotalMonths);
            var monthlyPayment = totalLoan * monthlyInterest / annuityFactor;

            var totalPayment = monthlyPayment * term.TotalMonths;
            var totalInterest = totalPayment - totalLoan;

            return new LoanCalculation(monthlyPayment, totalInterest, totalPayment);
        }
    }
}
