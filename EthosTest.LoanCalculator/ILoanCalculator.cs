﻿using System.Security.Cryptography.X509Certificates;
using EthosTest.LoanCalculator.Units;

namespace EthosTest.LoanCalculator
{
    /// <summary>
    /// Annuity loan payment calculator
    /// </summary>
    public interface ILoanCalculator
    {
        /// <summary>
        /// Perform the payment calculation for the loan with given parameters
        /// </summary>
        /// <param name="amount">Loan amount</param>
        /// <param name="interest">Yearly interest</param>
        /// <param name="downpayment">Downpayment</param>
        /// <param name="term">Term</param>
        /// <returns>Calculation result for the loan with given parameters</returns>
        ILoanCalculation CalculateLoan(decimal amount, Percent interest, decimal downpayment, Term term);
    }
}