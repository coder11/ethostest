﻿namespace EthosTest.LoanCalculator
{
    internal class LoanCalculation : ILoanCalculation
    {
        public LoanCalculation(decimal monthlyPayment, decimal totalInterest, decimal totalPayment)
        {
            MonthlyPayment = monthlyPayment;
            TotalInterest = totalInterest;
            TotalPayment = totalPayment;
        }

        public decimal MonthlyPayment { get; }
        public decimal TotalInterest { get; }
        public decimal TotalPayment { get; }
    }
}