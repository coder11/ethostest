﻿namespace EthosTest.LoanCalculator
{
    /// <summary>
    /// Represents the loan calculation result
    /// </summary>
    public interface ILoanCalculation
    {
        /// <summary>
        /// MonthlyPayment
        /// </summary>
        decimal MonthlyPayment { get; }

        /// <summary>
        /// TotalInterest
        /// </summary>
        decimal TotalInterest { get; }

        /// <summary>
        /// TotalPayment
        /// </summary>
        decimal TotalPayment { get; }
    }
}