# Ethos Lending Coding Challenge

Congratulations you have been selected to take the Ethos Lending coding challenge.  We pride ourselves on hiring top notch engineers and we are excited to see an example of your code!

## The problem
You are in charge of creating a loan payment calculator.  A lot of people want to figure out given a loan, downpayment and interest rate what their payments are going to look like and how much of them is going to go towards principle vs interest.  You program is going to get input from the command line in the following format:

```
amount: 100000
interest: 5.5%
downpayment: 20000
term: 30

```
NOTE: the last line of input is a blank line.
The term is givne in years.  The interest can be given a percentage or a digit.

Your program needs to process this input including the best way to handle some human errors (upper/lower case, spacing etc) and output a JSON of payment and total interest paid.

```
{
    "monthly payment": 454.23,
    "total interest": 83523.23
    "total payment" 163523.23
}
```

## Solution Framework

1. This challenge should take no more than 2-3 hours.  If it's taking longer chances are that you are solving something bigger than what we have asked for.
1. Please send us back an archive via some sharing method (don't send a zip file over) which contains your solution with instructions of how to get it running on either Mac or Windows.  Please do not use any tools that require purchase or subscription.  We will most likely test this on a bare bones Mac or Windows machine and just execute it on command line.
1. It is preferred that if the dependencies are needed that you include requirements in a file rather than sending us the libraries, so please utilize npm, nuget, pip or whatever is appropriate.

