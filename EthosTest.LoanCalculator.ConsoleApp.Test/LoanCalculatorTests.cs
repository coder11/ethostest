﻿using System.IO;
using System.Text;
using EthosTest.LoanCalculator.Units;
using NUnit.Framework;

namespace EthosTest.LoanCalculator.ConsoleApp.Test
{
    
    class LoanInputReaderTests
    {
        [Test]
        public void TheTest()
        {
            // arrange
            var testinput = @"100000
5,5%
20000
30
";

            using (var inputStream = new MemoryStream(Encoding.UTF8.GetBytes(testinput ?? "")))
            using (var outputStream = new MemoryStream())
            using (var reader = new LoanInputReader(inputStream, outputStream))
            {
                // act 
                reader.ReadInputs();

                // assert
                Assert.That(reader.Amount, Is.EqualTo(100000));
                Assert.That(reader.Intereset, Is.EqualTo(Percent.FromPercent(5.5m)));
                Assert.That(reader.Downpayment, Is.EqualTo(20000));
                Assert.That(reader.Term, Is.EqualTo(Term.FromYears(30)));
            }
        }
    }
}
